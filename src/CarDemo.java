import enums.FuelType;
import enums.Transmission;

public class CarDemo {


    public static void main(String[] args) {
        Car sedanCar = new Car.Builder("SEDAN")
                .seats(4)
                .doors(4)
                .fuelType(FuelType.DIESEL)
                .build();


        Car sportsCar = new Car.Builder("SPORTS")
                .seats(2)
                .doors(2)
                .transmission(Transmission.MANUAL)
                .engine("6.0L V12 DOHC and variable valve timing")
                .fuelType(FuelType.PETROL)
                .build();


        System.out.println(sedanCar);
        System.out.println();

        System.out.println(sportsCar);
    }
}
