import enums.FuelType;
import enums.Transmission;

public class Car {
    private final String carType;
    private Transmission transmission;
    private String engine;
    private int seats;
    private int doors;
    private FuelType fuelType;


    public Transmission getTransmission() {
        return transmission;
    }

    public String getEngine() {
        return engine;
    }

    public int getSeats() {
        return seats;
    }

    public int getDoors() {
        return doors;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    private Car(Builder builder) {
        this.carType = builder.carType;
        this.transmission = builder.transmission;
        this.engine = builder.engine;
        this.seats = builder.seats;
        this.doors = builder.doors;
        this.fuelType = builder.fuelType;
    }

    public static class Builder {
        private String carType;
        private Transmission transmission;
        private String engine;
        private int seats;
        private int doors;
        private FuelType fuelType;


        public Builder(String carType) {
            this.carType = carType;
        }

        public Builder transmission(Transmission transmission){
            this.transmission = transmission;
            return this;
        }

        public Builder engine(String engine){
            this.engine = engine;
            return this;
        }

        public Builder seats(int seats){
            this.seats = seats;
            return this;
        }

        public Builder doors(int doors){
            this.doors = doors;
            return this;
        }

        public Builder fuelType(FuelType fuelType){
            this.fuelType = fuelType;
            return this;
        }

        public Car build(){
            return new Car(this);
        }
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("--------------"+carType+"--------------------- \n")
                .append(" Engine: ")
                .append(engine)
                .append("\n enums.Transmission: ")
                .append(transmission)
                .append("\n Number of Doors: ")
                .append(doors)
                .append("\n Number of Seats: ")
                .append(seats)
                .append("\n Fuel Type: ")
                .append(fuelType);

        return sb.toString();
    }
}
